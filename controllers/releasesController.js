const express = require('express');
const {validationResult} = require('express-validator/check');
const Project = require('../models/project');
const Task = require('../models/task');
const Release = require('../models/release');

  function newRelease(req, res, next){
    const errors = validationResult(req);
    if(!errors.isEmpty()){
      return res.status(422).json({
        errors:errors.array()
      });
    }

    const data = req.body;
    if(!data.description){
        return res.status(500).json({
          message: 'Debes de completar todos los campos!'
        });
      }

    let release = new Release({
      _user: req.params.id,
      _description:data.description,
      _project: req.params.id
    });

    release.save()
    .then((obj)=>{

      Project.findById(req.params.id)
      .then((project)=>{

        Task.find({ _project: req.params.id})
        .then((tasks)=>{

          Release.find({ _project: req.params.id })
          .then((releases)=>{
            res.render('projects', {
              project : project,
              tasks : tasks,
              releases : releases
            });
          })
          .catch(()=>{
            res.status(500).json({
              errors:[{message:'Algo salio mal'}],
              data:err
            });
          });
        })
        .catch((err)=>{
          res.status(500).json({
            errors:[{message:'Algo salio mal'}],
            data:err
          });
        });
      })
      .catch((err)=>{
        res.status(500).json({
          errors:[{message:'Algo salio mal'}],
          data:err
        });

      });
    })
    .catch((err)=>{
      return res.status(500).json({
        errors:[{message:'Algo salio mal!!'}],
        data: err
      });
    });

  };

  function deleteRelease(req, res, next){
    //res.render('../views/');
  };

  function updateRelease(req, res, next){
    //res.render('../views/');
  };

  module.exports = {
  newRelease,
  deleteRelease,
  updateRelease
  };
