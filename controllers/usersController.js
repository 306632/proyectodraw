const express = require('express');
const User = require('../models/user');
const bcrypt = require('bcrypt-nodejs');

  function viewProfile(req, res, next){
    // To do: if is my profile
    res.render('../views/profile');
    // If is someone elses
    // Show modal
  };

  function logp(req, res, next) {
    res.render('../views/login');
  }

  function login(req, res, next) {

    const logdos = req.body;

    User.findOne({
      _username:logdos.username
    }, (err, user)=>{
      if(err){
        return res.status(500).json({message:'Error'});
      }if(!user){
        return res.status(500).json({message:'Usuario incorrecto!!'});
      }bcrypt.compare(logdos.password, user.password, (err, iguales)=>{
        if(err){
          return res.status(500).json({message:'Error!!'});
        }if(!iguales){
          return res.status(500).json({message:'Contraseña incorrecta!!'});
        }
      })
    }).then((objs)=>{
      if(!objs){
        return res.status(500).json({message:'Usuario o contrasena no validos!!'});
      }
    res.redirect('/');
    })
  };

  function signup(req, res, next) {
    const log = req.body;
    console.log(log.password);
    //if(!log.name ||!log.lastName ||log.username[1].length==0 ||!log.email ||log.password[1].length==0){
    if(!log.name ||!log.lastName ||!log.username ||!log.email ||! log.password){
      return res.status(500).json({
        message:'Debes completar todos los campos!!'
      });
    }

    let user = new User({
      name: log.name,
      lastName: log.lastName,
      username: log.username,
      email: log.email,
      password: log.password
    });

    User.find({
      $or: [
        {_email: user.email},
        {_username: user.username}
      ]
    }).exec((err,foundedUsers) => {
      if(err){
        return res.status(500).json({message:'Error'});
      }if(foundedUsers && foundedUsers.length >0){
        return res.status(500).json({message:'El usuario o correo ya esta registrado:('});
      }
      bcrypt.hash(log.password, null, null, (err, hash)=>{
        user.password = hash;
        user.save((err,newUser)=>{
          if(err) return res.status(500).json({message:'Hubo un error en la peticion'});
            if(newUser){
              res.redirect('/profile/');
            }
        });
      });
    });
  };

  function updateProfile(req, res, next) {
    //res.render('../views/profile');

  }

  function checkUsersInProject(req, res, next) {
    // To do
  };

  function checkUsersInTask(req, res, next) {
    // To do
  };

  function addToProject(req, res, next) {
    // To do
  };

  function deleteProfile(req, res, next) {
    //res.render('../views/signin');
  };

  function deleteFromProject(req, res, next) {
    // To do
  };

  module.exports = {
    viewProfile,
    updateProfile,
    logp,
    login,
    signup,
    checkUsersInProject,
    addToProject,
    deleteFromProject,
    deleteProfile,
    checkUsersInTask
  };
