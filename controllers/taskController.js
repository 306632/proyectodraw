const express = require('express');
const {validationResult} = require('express-validator/check');
const Project = require('../models/project');
const Users = require('../models/user');
const Task = require('../models/task');
const Release = require('../models/release');

  function newTask(req, res, next){
    const errors = validationResult(req);
    if(!errors.isEmpty()){
      return res.status(422).json({
        errors:errors.array()
      });
    }

    const data = req.body;
    if(!data.narrative || !data.priority || !data.asRole || !data.size ||
      !data.want || !data.suchWay || !data.criteria || !data.dice || !data.when
      || !data.so){
        return res.status(500).json({
          message: 'Debes de completar todos los campos!'
        });
      }

    let task = new Task({
      _narrative:data.narrative,
      _priority:data.priority,
      _asRole: req.params.id,
      _size:data.size,
      _want:data.want,
      _suchWay:data.suchWay,
      _criteria:data.criteria,
      _dice:data.dice,
      _when:data.when,
      _so:data.so,
      _state:data.state,
      _project:req.params.id
    });

    task.save()
    .then((obj)=>{
      Project.findById(req.params.id)
      .then((project)=>{

        Task.find({ _project: req.params.id})
        .then((tasks)=>{

          Release.find({ _project: req.params.id})
          .then((releases)=>{
            res.render('projects', {
              project : project,
              tasks : tasks,
              releases : releases
            });
          })
          .catch(()=>{
            res.status(500).json({
              errors:[{message:'Algo salio mal'}],
              data:err
            });
          });
        })
        .catch((err)=>{
          res.status(500).json({
            errors:[{message:'Algo salio mal'}],
            data:err
          });
        });
      })
      .catch((err)=>{
        res.status(500).json({
          errors:[{message:'Algo salio mal'}],
          data:err
        });
      });
    })
    .catch((err)=>{
      return res.status(500).json({
        errors:[{message:'Algo salio mal!!'}],
        data: err
      });
    });

  };

  function deleteTask(req, res, next){
    //res.render('../views/');
  };

  function updateTask(req, res, next){
    //res.render('../views/');
  };

  function aproveTask(req, res, next){
    //res.render('../views/');
  };

  function viewTask(req, res, next){
    //res.render('../views/');
  };

  function moveTask(req, res, next){
    //res.render('../views/');
  };

  module.exports = {
  newTask,
  deleteTask,
  updateTask,
  aproveTask,
  viewTask,
  moveTask
  };
