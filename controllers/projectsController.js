const express = require('express');
const Project = require('../models/project');
const Users = require('../models/user');
const Task = require('../models/task');
const Release = require('../models/release');
const {validationResult} = require('express-validator/check');

  function newProject(req, res, next){
    Users.find({})
    .then((objs)=>{
      res.render('../views/newproject', {
        users : objs
      });
    })
    .catch(()=>{
      res.status(500).json({
        errors:[{message:'Algo salio mal'}],
        data:[]
      });
    });
  };

  function create(req, res, next){

    const errors = validationResult(req);
    if(!errors.isEmpty()){
      return res.status(422).json({
        errors:errors.array()
      });
    }

    const form = req.body;
    if(!form.name ||!form.requestDate ||!form.startDate ||!form.description ||! form.scrumMaster ||! form.productOwner/* || form.team.length == '0'*/){
      return res.status(500).json({
        message:'Debes completar todos los campos!!'
      });
    }

    console.log(req.body.team);

    let project = new Project({
      _name:req.body.name,
      _requestDate:req.body.requestDate,
      _startDate:req.body.startDate,
      _description:req.body.description,
      _scrumMaster:req.body.scrumMaster,
      _productOwner:req.body.productOwner,
      _team:req.body.team
    });

    Project.find({
      _name: project.name,
    }).exec((err,foundedProjects) => {
      if(err){
        return res.status(500).json({message:'Error'});
      }if(foundedProjects && foundedProjects.length >0){
        return res.status(500).json({message:'Este nombre de proyecto ya esta ocupado.'});
      }

    project.save()
      .then((obj)=>{
        return res.status(500).json({
          message: obj
        });
      })
      // .then((obj)=>{
      //   res.redirect('/');
      // })
      .catch((err)=>{
        return res.status(500).json({
          errors:[{message:'Algo salio mal!!'}],
          data:err
        });
      });
    });
  };

  function listAll(req, res, next) {

    let page = req.params.page ? req.params.page : 1;

    const options = {
      page: 1,
      limit: 20,
      select: '_name _requestDate _startDate _description _scrumMaster _productOwner _team'
    }

    Project.find({}, (err, projects)=>{
      res.render('home', {
        projects : projects
      });
    })
    .catch((err)=>{
      res.status(500).json({
        errors: [{message: 'Algo salió mal.'}],
        data: err
      });
    });
  };

  function updateProject(req, res, next) {
      //res.render('');
  };
  function deleteProject(req, res, next) {
      //res.render('');
  }

  function viewProject(req, res, next) {

    Project.findById(req.params.id)
    .then((obj)=>{

      Task.find({ _project: obj.id})
      .then((tasks)=>{

        Release.find({ _project: obj.id })
        .then((releases)=>{
          res.render('projects', {
            project : obj,
            tasks : tasks,
            releases : releases
          });
        })
        .catch(()=>{
          res.status(500).json({
            errors:[{message:'Algo salio mal'}],
            data:err
          });
        });
      })
      .catch((err)=>{
        res.status(500).json({
          errors:[{message:'Algo salio mal'}],
          data:err
        });
      });
    })
    .catch((err)=>{
      res.status(500).json({
        errors:[{message:'Algo salio mal'}],
        data:err
      });

    });
  }

  module.exports = {
  newProject,
  create,
  listAll,
  updateProject,
  deleteProject,
  viewProject
  };
