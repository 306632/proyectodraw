const app = new Vue({
  el:'#app',
  data:{
    //Objetos que contengan informacion de la app (modelos)
    users:[

    ],
    user:{
      name: 'Carolina',
      lastName: 'Carranza'
    },
    operators:{
      n1:0,
      n2:0,
      res:0
    }
  },
  methods:{
    //Todas las funciones comunes de la app
    sum: function(event){
      this.operators.res = this.operators.n1 + this.operators.n2;
    }
  },
  computed:{
    //Funciones que pueden ser desplegables en las vistas solo si regresan el resultado
    result(){
      return this.operators.n1 + this.operators.n2;
    }
  },
  created(){
    //Aqui se ejecuta codigo al iniciarlizar la app
    fetch('/users/get/')
    .then(response => responde.json())
    .then(json =>{
      this.users = json.data.docs;
    });
  }
});
