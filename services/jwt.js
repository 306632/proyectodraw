'use strict'
const jwt = require('jwt-simple');
const moment = require('moment');

const llavesecreta = 'Mi primer llave';
exports.createToken = function(user){
  var token = {
    id:user._id,
    name:user._name,
    lastName:user._lastName,
    username:user._username,
    email:user._email,
    creation_date:moment().unix(),
    expiration_date:moment().add(1,'days').unix()
  };
  return jwt.encode(token, llavesecreta);
}
