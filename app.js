const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const passportSetup = require('./config/passport-setup');

passport = require('passport');
const mongoose = require('mongoose');
const keys = require('./config/keys');
const cookieSession = require('cookie-session');


const indexRouter = require('./routes/index');
const LoginSignupRouter = require('./routes/LoginSignup');
const usersRouter = require('./routes/users');
const projectsRouter = require('./routes/projects');
const profileRouter = require('./routes/profile');
const taskRouter = require('./routes/task');
const releaseBacklogRouter = require('./routes/release');
const sprintBacklogRouter = require('./routes/sprintBacklog');

const app = express();

const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
// parse application/json
app.use(bodyParser.json());
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// parse the raw data
app.use(bodyParser.raw());
// parse text
app.use(bodyParser.text());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(cookieSession({
  maxAge: 24 * 60 * 60 * 1000,
  keys:[keys.session.cookieKey]
}));
app.use(passport.initialize());
//app.use(passport.session());

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/LoginSignup', LoginSignupRouter);
app.use('/projects', projectsRouter);
app.use('/profile', profileRouter);
app.use('/task', taskRouter);
app.use('/release', releaseBacklogRouter);
app.use('/sprintBacklog', sprintBacklogRouter);

app.get('/', (req, res) => {
  res.render('home', {user: req.user});
});



// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
