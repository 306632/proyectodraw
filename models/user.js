const mongoose = require('mongoose');
//require('/models/userskills');
const mongoosePaginate = require('mongoose-paginate-v2');
const Schema = mongoose.Schema,
  ObjectId = Schema.ObjectId;

const schema = new Schema({
  //_id:ObjectId,
  _name:String,
  _lastName:String,
  _username:String,
  _email:String,
  _password:String,
  _birthdate:{type:Date},
  _curp:String,
  _rfc:String,
  _address:String,
  _googleid:String
});

class User {

  constructor(id, name, lastName, username, email, password, birthdate, curp, rfc, address){
    this._id = id;
    this._name = name;
    this._lastName = lastName;
    this._username = username;
    this._email = email;
    this._password = password;
    this._birthdate = birthdate;
    this._curp = curp;
    this._rfc = rfc;
    this._address = address;
  }

  get id(){
    return this._id;
  }

  set id(v){
    this._id = v;
  }

  get fullName(){
    return `${this._name} ${this._lastName}`;
  }

  get name(){
    return this._name;
  }

  set name(v){
    this._name = v;
  }

  get lastName(){
    return this._lastName;
  }

  set lastName(v){
    this._lastName = v;
  }

  get username(){
    return this._username;
  }

  set username(v){
    this._username = v;
  }

  get email(){
    return this._email;
  }

  set email(v){
    this._email = v;
  }

  get password(){
    return this._password;
  }

  set password(v){
    this._password = v;
  }

  get birthdate(){
    return this._birthdate;
  }

  set birthdate(v){
    this._birthdate = v;
  }

  get curp(){
    return this._curp;
  }

  set curp(v){
    this._curp = v;
  }

  get rfc(){
    return this._rfc;
  }

  set rfc(v){
    this._rfc = v;
  }

  get address(){
    return this._address;
  }

  set address(v){
    this._address = v;
  }
}

schema.plugin(mongoosePaginate);
schema.loadClass(User);
module.exports = mongoose.model('User', schema);
