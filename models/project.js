const mongoose = require('mongoose');
const Schema = mongoose.Schema;
require ('../models/user');
const User = mongoose.model('User');
const mongoosePaginate = require('mongoose-paginate-v2');

const schema = Schema({
  _name: String,
  _requestDate: String,
  _startDate: String,
  _description: String,
  _scrumMaster:String,
  _productOwner: String,
  _team: [{ type: Schema.Types.ObjectId, ref: 'User' }]
});

class Project {

  constructor(name, requestDate, startDate, description, scrumMaster, productOwner, team){
    this._name = name;
    this._requestDate = requestDate;
    this._startdate = startDate;
    this._description = description;
    this._scrumMaster = scrumMaster;
    this._productOwner = productOwner;
    this._team = team;
  }

  get name(){
    return this._name;
  }

  set name(v){
    this._name = v;
  }

  get requestDate(){
    return this._requestDate;
  }

  set requestDate(v){
    this._requestDate = v;
  }

  get startDate(){
    return this._startdate;
  }

  set startDate(v){
    this._startdate = v;
  }

  get description(){
    return this._description;
  }

  set description(v){
    this._description = v;
  }

  get scrumMaster(){
    return this._scrumMaster;
  }

  set scrumMaster(v){
    this._scrumMaster = v;
  }

  get productOwner(){
    return this._productOwner;
  }

  set productOwner(v){
    this._productOwner = v;
  }

  get team(){
    return this._team;
  }

  set team(v){
    this._team = v;
  }
}

schema.plugin(mongoosePaginate);
schema.loadClass(Project);
module.exports = mongoose.model('Project', schema);
