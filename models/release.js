const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
require('../models/user');
require('../models/project');
const User = mongoose.model('User');
const Project = mongoose.model('Project');

const Schema = mongoose.Schema,
  ObjectId = Schema.ObjectId;

const schema = Schema({
  _user: {type: ObjectId, ref: 'User'},
  _description: String,
  _project: {type: ObjectId, ref: 'Project'},
});

class Release{

  constructor(description) {
    this._description = description;
  }

  get description(){
    return this._description;
  }

  set description(value){
    this._description = value;
  }

}

schema.plugin(mongoosePaginate);
schema.loadClass(Release);
module.exports = mongoose.model('Release', schema);
