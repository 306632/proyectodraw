const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
require('../models/user');
require('../models/project');
const User = mongoose.model('User');
const Project = mongoose.model('Project');

const Schema = mongoose.Schema,
  ObjectId = Schema.ObjectId;

const schema = Schema({
  _narrative:String,
  _priority:Number,
  _asRole: {type: ObjectId, ref: 'User'},
  _size:Number,
  _want:String,
  _suchWay:String,
  _criteria:String,
  _dice:String,
  _when:String,
  _so:String,
  _state:String,
  _project: {type: ObjectId, ref: 'Project'}
});

class Task{

  constructor(narrative, priority, asRole, size, want, suchWay, criteria, dice, when, so, state) {
    this._narrative = narrative;
    this._priority = priority;
    this._asRole = asRole;
    this._size = size;
    this._want = want;
    this._suchWay = suchWay;
    this._criteria = criteria;
    this._dice = dice;
    this._when = when;
    this._so = so;
    this._state = state;
  }

  get narrative(){
    return this._narrative;
  }

  set narrative(value){
    this._narrative = value;
  }

  get priority(){
    return this._priority;
  }

  set priority(value){
    this._priority = value;
  }

  get asRole(){
    return this._asRole;
  }

  set asRole(value){
    this._asRole = value;
  }

  get size(){
    return this._size;
  }

  set size(value){
    this._size = value;
  }

  get want(){
    return this._want;
  }

  set want(value){
    this._want = value;
  }

  get suchWay(){
    return this._suchWay;
  }

  set suchWay(value){
    this._suchWay = value;
  }

  get criteria(){
    return this._criteria;
  }

  set criteria(value){
    this._criteria = value;
  }

  get dice(){
    return this._dice;
  }

  set dice(value){
    this._dice = dice;
  }

  get when(){
    return this._when;
  }

  set when(value){
    this._when = value;
  }

  get so(){
    return this._so;
  }

  set so(value){
    this._so = value;
  }

  get state(){
    return this._so;
  }

  set state(value){
    this._so = value;
  }
}

schema.plugin(mongoosePaginate);
schema.loadClass(Task);
module.exports = mongoose.model('Task', schema);
