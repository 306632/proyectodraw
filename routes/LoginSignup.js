var express = require('express');
var router = express.Router();
const passport = require('passport');
//const authentication = require('../middleware/authentication');
const usersController=require('../controllers/usersController');
router.get('/logout', (req, res) =>{
  req.logout();
  res.redirect('/LoginSignup');
})
router.post('/login', usersController.login);
router.post('/', usersController.signup);
/* GET nuevahistoria page. */
router.get('/', usersController.logp);
//GOOGlE
router.get('/google', passport.authenticate('google', {
  scope:['profile']
  }));
  //Callback for Google
  router.get('/google/redirect', passport.authenticate('google'), (req, res) => {
    res.redirect('/');
  });
  //Twitter
router.get('/twitter', passport.authenticate('twitter', {
  scope:['profile']
  }));
  router.get('/dropbox', passport.authenticate('dropbox', {
    scope:['profile']
    }));


module.exports = router;
