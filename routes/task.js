var express = require('express');
var router = express.Router();

const taskController=require('../controllers/taskController');

/* GET nuevahistoria page. */
router.post('/:id', taskController.newTask);

router.get('/:id', taskController.viewTask);

module.exports = router;
