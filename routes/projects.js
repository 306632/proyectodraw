var express = require('express');
var router = express.Router();

const projectsController=require('../controllers/projectsController');

/* GET nuevoexpediente page. */
// router.get('/', projectsController.viewProject);

router.get('/:id', projectsController.viewProject);

// router.post('/task/:id', projectsController.newTask);

module.exports = router;
