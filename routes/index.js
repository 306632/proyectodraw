var express = require('express');
var router = express.Router();
const projectsController = require('../controllers/projectsController');
const usersController=require('../controllers/usersController');
const { check, body, params} = require('express-validator/check');

router.post('/create', projectsController.create);

router.get('/', usersController.logp);

router.get('/newproject', projectsController.newProject);

module.exports = router;
