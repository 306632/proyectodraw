var express = require('express');
var router = express.Router();

const usersController = require('../controllers/usersController');

/* GET users listing. */

router.put('/updateProfile/:id', usersController.updateProfile);

router.get('/project/:id', usersController.checkUsersInProject);

router.post('/project/:id/', usersController.addToProject);

router.delete('/profile/:id', usersController.deleteProfile);

router.delete('/project/:id', usersController.deleteFromProject);

module.exports = router;
