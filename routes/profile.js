var express = require('express');
var router = express.Router();

const usersController=require('../controllers/usersController');

/* GET informacionpersonal page. */
router.get('/', usersController.viewProfile);

module.exports = router;
