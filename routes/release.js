var express = require('express');
var router = express.Router();

const releasesController = require('../controllers/releasesController');

/* GET tablerocontrol page. */
router.post('/:id', releasesController.newRelease);

module.exports = router;
