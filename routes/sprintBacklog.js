var express = require('express');
var router = express.Router();

const scrumController=require('../controllers/scrumController');

/* GET tablerocontrol page. */
router.get('/', scrumController.viewSbacklog);

module.exports = router;
