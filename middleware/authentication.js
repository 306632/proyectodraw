const jwt = require('jwt-simple');
const moment = require('moment');

const llavesecreta = 'Mi primer llave';

exports.authentication = function(req, res, next){
  if(!req.headers.authorization){
    return res.status(403).json({message: 'No hay cabecera de autentificacion'});
  }
  var token = req.headers.authorization.replace(/['"]+/g, '');

  try{
    var info = jwt.decode(token, llavesecreta);
    if(info.expiration_date<=moment().unix()){
      return res.status(500).json({message: 'Sesion expirada'});
    }
  }catch(ex){
    return res.status(500).json({message: 'Token no valido'});
  }

  req.user = info;
  next();

}
