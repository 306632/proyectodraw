const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20');
const TwitterStrategy = require('passport-twitter').Strategy;
const DropBoxStrategy = require('passport-dropbox-oauth2').Strategy;
const keys = require('./keys');
const User = require('../models/user');
passport.serializeUser((user, done) => {
  done(null, user.id);
});
passport.deserializeUser((id, done) => {
  Usuario.findById(id).then((user) =>{
    done(null, user);
  })
  done(null, user.id);
});

passport.use(
  new GoogleStrategy({
  //Options for the google strategy
  callbackURL:'https://app-proyecto2018.herokuapp.com/LoginSignup/google/redirect',
  clientID:keys.google.clientID,
  clientSecret:keys.google.clientSecret
}, (accessToken, refreshToken, profile, done) => {
  //Passport callback function
  User.findOne({_googleid:profile.id}).then((currentUser) => {
    if(currentUser){
      //Si ya existe el Usuario
      console.log('User is:'.currentUser);
      done(null, currentUser)
    }else{
      //Crear un usuario nuevo si no existe
      new User({
        _name: profile.displayName,
        _googleid:profile.id

      }).save().then((newUser) =>{
        console.log('nuevo usuario registrado' + newUser);
        done(null, newUser);
      });
    }
  });

  })
);
passport.use(
  new TwitterStrategy({
  //Options for the google strategy
  callbackURL:'/LoginSignup/twitter/redirect',
  consumerKey:keys.twitter.consumerKey,
  consumerSecret:keys.twitter.consumerSecret
  }, () => {
  //Passport callback function

  })
);
passport.use(
  new DropBoxStrategy({
  //Options for the google strategy
  callbackURL:'/LoginSignup/dropbox/redirect',
  clientID:keys.dropbox.clientID,
  clientSecret:keys.dropbox.clientSecret
  }, () => {
  //Passport callback function

  })
);
